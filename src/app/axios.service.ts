import { baseURL } from './global';
import { Injectable, OnInit } from '@angular/core';
import axios from 'axios';
import Swal from 'sweetalert2';
import { Subject } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';

@Injectable({
  providedIn: 'root'
})
export class AxiosService {
  showLoader = new Subject();
  auth: any = new Subject();
  token: string = null;
  axios: any;

  
  constructor(private spinner: NgxSpinnerService) {
    this.axios = axios.create({
      baseURL: baseURL,
      headers: {
       // "Content-Type": "application/x-www-form-urlencoded",
              'content-type': 'application/json'
      },
      withCredentials: false
    });
    this.axios.interceptors.request.use((req) => this.handleRequest(req));
    this.axios.interceptors.response.use((res) => this.handleResponse(res), (rej) => this.handleError(rej));
  }
  private handleError = (error: any) => {
    this.spinner.hide();
    error = { ...error };
    if (error.code) {
      return undefined;
    }
    const response = error.response;
    if (!response) {
      return undefined;
    }
    const status = response.status;
    const message = response.data.message;

    return error.response.data;
  }
  private handleResponse = (res: any) => {
    this.spinner.hide();
    const status = res.status;
    const message = res.data.message || 'Success';
    return res.data;
  }
  private handleRequest(req) {
    // this.token = localStorage.getItem('openLoopToken');
    this.spinner.show();
    return req;
  }
  logout = () => {
    localStorage.clear();
    // this.loginToken = 'fbb6ee477adaa5cb6c92';
    // this.setAuthToken(this.loginToken);
    // this.router.navigate(['/auth/login']);
  };
  ngOnInit() {
    // if (localStorage.getItem('TQMI_TKN')) {
    //   this.token = localStorage.getItem('TQMI_TKN');
    //   this.setAuthToken(localStorage.getItem('TQMI_TKN'));
    // }
  }
}
