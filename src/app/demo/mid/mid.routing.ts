import { Routes } from '@angular/router';

import { MidListComponent } from './mid-list/mid-list.component';
import { MidAddComponent } from './mid-add/mid-add.component';
import { MidViewComponent } from './mid-view/mid-view.component';
import { MidEditComponent } from './mid-edit/mid-edit.component';

export const MidRoutes: Routes = [
    {

      path: '',
      children: [ {
        path: 'mid-list',
        component: MidListComponent
    },{
        path: 'mid-add',
        component: MidAddComponent
    },{
        path: 'mid-edit',
        component: MidEditComponent
    },{
        path: 'mid-view',
        component: MidViewComponent
    }]
}
];