import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MidRoutes } from './mid.routing';
import {SharedModule} from '../../theme/shared/shared.module';
import {DataTablesModule} from 'angular-datatables';
import {NgbDatepickerModule} from '@ng-bootstrap/ng-bootstrap';
import { MidViewComponent } from './mid-view/mid-view.component';
import { MidEditComponent } from './mid-edit/mid-edit.component';
import { MidListComponent } from './mid-list/mid-list.component';
import { MidAddComponent } from './mid-add/mid-add.component';
import {NgxPaginationModule} from 'ngx-pagination';
@NgModule({
  declarations: [MidViewComponent, MidEditComponent, MidListComponent, MidAddComponent],
  imports: [
    CommonModule,
    NgxPaginationModule,
    RouterModule.forChild(MidRoutes),
    SharedModule,
    DataTablesModule,
    NgbDatepickerModule
  ]
})
export class MidModule { }
