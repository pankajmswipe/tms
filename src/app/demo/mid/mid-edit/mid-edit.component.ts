import { Component, OnInit } from '@angular/core';
import { NgbDate, NgbCalendar, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import axios from "axios";
import swal from 'sweetalert2';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/api.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-mid-edit',
  templateUrl: './mid-edit.component.html',
  styleUrls: ['./mid-edit.component.scss']
})
export class MidEditComponent implements OnInit {
  Enabled='Y';
  LegalName:'';
  DBAName:'';
  City:'';
  Country:'';
  MobileNumber:'';
  PostalCode:'';
  MCC:'';
  StartHours:'';
  EndHours:'';
  SuperMID='GC0000000011111';
  VoidPWD:'';
  PACPWD:'';
  MID:'';
  
 constructor( private router: Router,private _apiService: ApiService, private spinner: NgxSpinnerService) { }
  ngOnInit() {
    const responseData = JSON.parse(localStorage.getItem('midDetails'));
    console.log(responseData);
    this.Enabled=responseData.enabled;
    this.LegalName=responseData.legalname;
    this.DBAName=responseData.dbaname;
    this.City=responseData.city;
    this.Country=responseData.country;
    this.MobileNumber=responseData.mobilenumber;
    this.PostalCode=responseData.postalcode;
    this.MCC=responseData.mcc;
    this.StartHours=responseData.starthours;
    this.EndHours=responseData.endhours;
    this.SuperMID=responseData.supermid;
    this.VoidPWD=responseData.voidpassword;
    this.PACPWD=responseData.pacpassword;
    this.MID=responseData.mid;
  }
  async InsertMIDDetails(){
    const obj = {
      "insTMSMIDTable": [
        {
          "mid": this['MID'],
          "legalname": this['LegalName'],
          "dbaname": this['DBAName'],
          "city": this['City'],
          "country": this['Country'],
          "mobilenumber": this['MobileNumber'],
          "postalcode": this['PostalCode'],
          "mcc": this['MCC'],
          "enabled": this['Enabled'],
          "starthours":this['StartHours'],
          "endhours": this['EndHours'],
          "supermid": this['SuperMID'],
          "voidpassword": this['VoidPWD'],
          "pacpassword": this['PACPWD']
        }
      ],
        "operation": "U"
      }
    const response: any = await this._apiService.InsertMIDDetails(obj);
    if (response.status=true) {
       swal.fire({
        type: 'success',
        text: response.responseMessage
       })
    }else{
      swal.fire({
        type: 'error',
        text: response.responseMessage
       })
    }
  }

}

