import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MidEditComponent } from './mid-edit.component';

describe('MidEditComponent', () => {
  let component: MidEditComponent;
  let fixture: ComponentFixture<MidEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MidEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MidEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
