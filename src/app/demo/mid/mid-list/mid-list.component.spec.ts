import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MidListComponent } from './mid-list.component';

describe('MidListComponent', () => {
  let component: MidListComponent;
  let fixture: ComponentFixture<MidListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MidListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MidListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
