import { Component, OnInit } from '@angular/core';
import { NgbDate, NgbCalendar, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import axios from "axios";
import swal from 'sweetalert2';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/api.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-mid-list',
  templateUrl: './mid-list.component.html',
  styleUrls: ['./mid-list.component.scss']
})
export class MidListComponent implements OnInit {
  appsMID;
  getMIDDetails:any=[];
  Enabled='Y';
  LegalName:'';
  DBAName:'';
  City:'';
  Country:'';
  MobileNumber:'';
  PostalCode:'';
  MCC:'';
  StartHours:'';
  EndHours:'';
  SuperMID:'';
  VoidPWD:'';
  PACPWD:'';
  MID:'';
  constructor( private router: Router,private _apiService: ApiService, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.GetMIDDetails();
  }
  addMID(){
    this.router.navigate(["/mid/mid-add"]);
  }
  async GetMIDDetails() {
    const response: any = await this._apiService.GetMIDDetails();
    if (response) {
      this.getMIDDetails = response.table;
    }
  }
  async viewMid(args){
    //console.log(args);
    this.MID = args.mid;
    this.LegalName = args.legalname;
    this.DBAName = args.dbaname;
    this.City = args.city;
    this.Country = args.country;
    this.MobileNumber = args.mobilenumber;
    this.PostalCode = args.postalcode;
    this.Enabled = args.enabled;
    this.MCC = args.mcc;
    this.StartHours = args.starthours;
    this.EndHours = args.endhours;
    this.SuperMID=args.supermid;
    this.VoidPWD=args.voidpassword;
    this.PACPWD=args.pacpassword;
  document.querySelector('#modal-2').classList.add('md-show');
}
closePopUpTID(){
    this.MID = '';
    this.LegalName = '';
    this.DBAName ='';
    this.City = '';
    this.Country = '';
    this.MobileNumber = '';
    this.PostalCode = '';
    this.Enabled = '';
    this.MCC = '';
    this.StartHours = '';
    this.EndHours = '';
    this.SuperMID='';
    this.VoidPWD='';
    this.PACPWD='';
  document.getElementById("modal-2").classList.remove("md-show");
}
async updateMid(args){
    localStorage.setItem('midDetails' , JSON.stringify(args));
    //console.log(args);
    this.router.navigate(['/mid' , 'mid-edit']);
  
}
 async deleteMid (args , index) {
  Swal.fire({
    title: 'Are you sure want to remove?',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, delete it!',
    cancelButtonText: 'No, keep it'
  }).then((result) => {
    if (result.value) {
      const itemDeleteId = args.mid;
      const obj = {
        "insTMSMIDTable": [
          {
            "mid": itemDeleteId,
            "legalname": "",
            "dbaname": "",
            "city": "",
            "country": "",
            "mobilenumber": "",
            "postalcode": "",
            "mcc": "",
            "enabled": "",
            "starthours": "",
            "endhours": "",
            "supermid": "",
            "voidpassword": "",
            "pacpassword": ""
          }
        ],
        "operation": "D"
      }
      const response: any = this._apiService.InsertMIDDetails(obj);
      
      if (response.status=true) {
        setTimeout (() => {
        
          this.GetMIDDetails();
          Swal.fire(
            response.responseMessage,
            'Success'
          );
       }, 1000);
       
        
      }
     } else if (result.dismiss === Swal.DismissReason.cancel) {
      Swal.fire(
        'Cancelled',
      )
    }
  })
}


}
