import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mid-view',
  templateUrl: './mid-view.component.html',
  styleUrls: ['./mid-view.component.scss']
})
export class MidViewComponent implements OnInit {
  Enabled='Y';
  LegalName:'';
  DBAName:'';
  City:'';
  Country:'';
  MobileNumber:'';
  PostalCode:'';
  MCC:'';
  StartHours:'';
  EndHours:'';
  SuperMID='GC0000000011111';
  VoidPWD:'';
  PACPWD:'';
  MID:'';
  constructor() { }

  ngOnInit() {
  }

}
