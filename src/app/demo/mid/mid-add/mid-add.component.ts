import { Component, OnInit } from '@angular/core';
import { NgbDate, NgbCalendar, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import axios from "axios";
import swal from 'sweetalert2';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/api.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-mid-add',
  templateUrl: './mid-add.component.html',
  styleUrls: ['./mid-add.component.scss']
})
export class MidAddComponent implements OnInit {
  Enabled='Y';
  LegalName:'';
  DBAName:'';
  City:'';
  Country:'';
  MobileNumber:'';
  PostalCode:'';
  MCC:'';
  StartHours:'';
  EndHours:'';
  SuperMID='GC0000000011111';
  VoidPWD:'';
  PACPWD:'';
  MID:'';
  constructor( private router: Router,private _apiService: ApiService, private spinner: NgxSpinnerService) { }
  async InsertMIDDetails() {
  const obj = {
    "insTMSMIDTable": [
      {
        "mid": this['MID'],
        "legalname": this['LegalName'],
        "dbaname": this['DBAName'],
        "city": this['City'],
        "country": this['Country'],
        "mobilenumber": this['MobileNumber'],
        "postalcode": this['PostalCode'],
        "mcc": this['MCC'],
        "enabled": this['Enabled'],
        "starthours":this['StartHours'],
        "endhours": this['EndHours'],
        "supermid": this['SuperMID'],
        "voidpassword": this['VoidPWD'],
        "pacpassword": this['PACPWD']
      }
    ],
      "operation": "I"
    }
    const response: any = await this._apiService.InsertMIDDetails(obj);
    if (response.status=true) {
       swal.fire({
        type: 'success',
        text: response.responseMessage
       })
    }else{
      swal.fire({
        type: 'error',
        text: response.responseMessage
       })
    }
  }
  ngOnInit() {
  }

}
