import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MidAddComponent } from './mid-add.component';

describe('MidAddComponent', () => {
  let component: MidAddComponent;
  let fixture: ComponentFixture<MidAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MidAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MidAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
