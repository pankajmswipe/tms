import { Component, OnInit } from '@angular/core';
import { NgbDate, NgbCalendar, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import axios from "axios";
import swal from 'sweetalert2';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/api.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-profile-list',
  templateUrl: './profile-list.component.html',
  styleUrls: ['./profile-list.component.scss']
})
export class ProfileListComponent implements OnInit {
  appsProfile;
  getProfileDetails:any=[];
  ProfileName:'';
  CurrencyCode='Y';
  Sale='Y';
  preauth='Y';
  preauthcom='Y';
  mentry='Y';
  tip='Y';
  ipp='Y';
  rcpt='Y';
  gpn='Y';
  amex='Y';
  mcard='Y';
  visa='Y';
  cup='Y';
  fullMask='Y';

  constructor( private router: Router,private _apiService: ApiService, private spinner: NgxSpinnerService) { }


  ngOnInit() {
    this.GetTMSProfileDetails();
  }
  addProfile(){
    this.router.navigate(["/profile/profile-add"]);
  }
  async GetTMSProfileDetails() {
    const response: any = await this._apiService.GetTMSProfileDetails();
    if (response) {
      this.getProfileDetails = response.table;
    }
  }
  async viewProfile(args){
     //console.log(args);
     this.ProfileName=args.profilename;
     this.CurrencyCode=args.currencycode;
     this.Sale=args.sale;
     this.preauth=args.preauth;
     this.preauthcom=args.preauthcompl;
     this.mentry=args.manualentry;
     this.tip=args.tip;
     this.ipp=args.ipp;
     this.rcpt=args.receiptrequired;
     this.gpn=args.gpn;
     this.amex=args.amex;
     this.mcard=args.mastercard;
     this.visa=args.visa;
     this.cup=args.cup;
     this.fullMask=args.fullmask;
   document.querySelector('#modal-4').classList.add('md-show');
  }
  closePopUpTID(){
    this.ProfileName='';
    this.CurrencyCode='';
    this.Sale='';
    this.preauth='';
    this.preauthcom='';
    this.mentry='';
    this.tip='';
    this.ipp='';
    this.rcpt='';
    this.gpn='';
    this.amex='';
    this.mcard='';
    this.visa='';
    this.cup='';
    this.fullMask='';
  document.getElementById("modal-4").classList.remove("md-show");
}
  async updateProfile(args){
    localStorage.setItem('profileDetails' , JSON.stringify(args));
    //console.log(args);
    this.router.navigate(['/profile' , 'profile-edit']);
  }
  async deleteProfile(args,index){
    Swal.fire({
      title: 'Are you sure want to remove?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        const itemDeleteId = args.profilename;
        const obj = {
          "insTMSProfileNmTable": [
            {
              "profilename": itemDeleteId,
              "currencycode": "string",
              "sale": "string",
              "preauth": "string",
              "preauthcompl": "string",
              "manualentry": "string",
              "tip": "string",
              "ipp": "string",
              "receiptrequired": "string",
              "gpn": "string",
              "amex": "string",
              "mastercard": "string",
              "visa": "string",
              "cup": "string",
              "fullmask": "string"
            }
          ],
          "operation": "D"
        }
        const response: any = this._apiService.InsertTMSProfileDetails(obj);
        
        if (response.status=true) {
          setTimeout (() => {
          
            this.GetTMSProfileDetails();
            Swal.fire(
              response.responseMessage,
              'Success'
            );
         }, 1000);
         
          
        }
       } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelled',
        )
      }
    })
  }
  async searchData(){
    alert('search');
  }


}
