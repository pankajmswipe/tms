import { Component, OnInit } from '@angular/core';
import { NgbDate, NgbCalendar, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import axios from "axios";
import swal from 'sweetalert2';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/api.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-profile-edit',
  templateUrl: './profile-edit.component.html',
  styleUrls: ['./profile-edit.component.scss']
})
export class ProfileEditComponent implements OnInit {
  ProfileName:'';
  CurrencyCode='Y';
  Sale='Y';
  preauth='Y';
  preauthcom='Y';
  mentry='Y';
  tip='Y';
  ipp='Y';
  rcpt='Y';
  gpn='Y';
  amex='Y';
  mcard='Y';
  visa='Y';
  cup='Y';
  fullMask='Y';
  constructor( private router: Router,private _apiService: ApiService, private spinner: NgxSpinnerService) { }
  async UpdateInsertTMSProfileDetails() {
  const obj = {
    "insTMSProfileNmTable": [
      {
        "profilename": this['ProfileName'],
        "currencycode": this['CurrencyCode'],
        "sale": this['Sale'],
        "preauth": this['preauth'],
        "preauthcompl": this['preauthcom'],
        "manualentry": this['mentry'],
        "tip": this['tip'],
        "ipp": this['ipp'],
        "receiptrequired": this['rcpt'],
        "gpn": this['gpn'],
        "amex": this['amex'],
        "mastercard": this['mcard'],
        "visa": this['visa'],
        "cup": this['cup'],
        "fullmask": this['fullMask']
      }
    ],
      "operation": "U"
    }
    const response: any = await this._apiService.InsertTMSProfileDetails(obj);
    if (response.status=true) {
       swal.fire({
        type: 'success',
        text: response.responseMessage
       })
    }else{
      swal.fire({
        type: 'error',
        text: response.responseMessage
       })
    }
  }
  ngOnInit() {
    const responseData = JSON.parse(localStorage.getItem('profileDetails'));
    console.log(responseData);
    this.ProfileName=responseData.profilename;
    this.CurrencyCode=responseData.currencycode;
    this.Sale=responseData.sale;
    this.preauth=responseData.preauth;
    this.preauthcom=responseData.preauthcompl;
    this.mentry=responseData.manualentry;
    this.tip=responseData.tip;
    this.ipp=responseData.ipp;
    this.rcpt=responseData.receiptrequired;
    this.gpn=responseData.gpn;
    this.amex=responseData.amex;
    this.mcard=responseData.mastercard;
    this.visa=responseData.visa;
    this.cup=responseData.cup;
    this.fullMask=responseData.fullmask;
  }
}