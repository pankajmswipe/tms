import { Routes } from '@angular/router';

import { ProfileListComponent } from './profile-list/profile-list.component';
import { ProfileAddComponent } from './profile-add/profile-add.component';
import { ProfileViewComponent } from './profile-view/profile-view.component';
import { ProfileEditComponent } from './profile-edit/profile-edit.component';

export const ProfileRoutes: Routes = [
    {

      path: '',
      children: [ {
        path: 'profile-list',
        component: ProfileListComponent
    },{
        path: 'profile-add',
        component: ProfileAddComponent
    },{
        path: 'profile-edit',
        component: ProfileEditComponent
    },{
        path: 'profile-view',
        component: ProfileViewComponent
    }]
}
];