import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile-view',
  templateUrl: './profile-view.component.html',
  styleUrls: ['./profile-view.component.scss']
})
export class ProfileViewComponent implements OnInit {
  ProfileName:'';
  CurrencyCode='Y';
  Sale='Y';
  preauth='Y';
  preauthcom='Y';
  mentry='Y';
  tip='Y';
  ipp='Y';
  rcpt='Y';
  gpn='Y';
  amex='Y';
  mcard='Y';
  visa='Y';
  cup='Y';
  fullMask='Y';
  Enabled;
  constructor() { }

  ngOnInit() {
  }

}
