
import { ProfileAddComponent } from './profile-add/profile-add.component';
import { ProfileListComponent } from './profile-list/profile-list.component';
import { ProfileEditComponent } from './profile-edit/profile-edit.component';
import { ProfileViewComponent } from './profile-view/profile-view.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ProfileRoutes } from './profile.routing';
import {SharedModule} from '../../theme/shared/shared.module';
import {DataTablesModule} from 'angular-datatables';
import {NgbDatepickerModule} from '@ng-bootstrap/ng-bootstrap';
import {NgxPaginationModule} from 'ngx-pagination';
@NgModule({
  declarations: [ProfileAddComponent, ProfileListComponent, ProfileEditComponent, ProfileViewComponent],
  imports: [
    CommonModule,
    NgxPaginationModule,
    RouterModule.forChild(ProfileRoutes),
    SharedModule,
    DataTablesModule,
    NgbDatepickerModule
  ]
})
export class ProfileModule { }
