import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupermidListComponent } from './supermid-list.component';

describe('SupermidListComponent', () => {
  let component: SupermidListComponent;
  let fixture: ComponentFixture<SupermidListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupermidListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupermidListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
