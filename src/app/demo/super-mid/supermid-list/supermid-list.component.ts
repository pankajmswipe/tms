import { Component, OnInit } from '@angular/core';
import { NgbDate, NgbCalendar, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import axios from "axios";
import swal from 'sweetalert2';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/api.service';
import { Router } from '@angular/router';
import {baseURL} from 'src/app/global';

@Component({
  selector: 'app-supermid-list',
  templateUrl: './supermid-list.component.html',
  styleUrls: ['./supermid-list.component.scss']
})
export class SupermidListComponent implements OnInit {
  getSuperMIDDetails:any=[];
  appsQueryPagep;
  Enabled='Y';
  LegalName:'';
  DBAName:'';
  Address:'';
  MobileNumber:'';
  EmailID:'';
  SuperMID:'';
  upDatedID:'';
  constructor( private router: Router,private _apiService: ApiService, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.GetSuperMIDDetails();
  }
  
  addSuperMID(){
    this.router.navigate(["/super-mid/supermid-add"]);
  }
  async GetSuperMIDDetails() {
    const response: any = await this._apiService.GetSuperMIDDetails();
    if (response) {
      this.getSuperMIDDetails = response.table;
    }
  }
  async viewSuperMID(args){
      console.log(args);
      this.LegalName = args.legalname;
      this.DBAName = args.dbaname;
      this.Address = args.address;
      this.MobileNumber = args.mobilenumber;
      this.EmailID = args.emailid;
      this.SuperMID = args.supermid;
      this.Enabled = args.enabled;
      this.upDatedID = args.id;
    document.querySelector('#modal-1').classList.add('md-show');
  }
  closePopUp(){
    this.LegalName = "";
    this.DBAName = "";
    this.Address = "";
    this.MobileNumber = "";
    this.EmailID = "";
    this.SuperMID ="";
    this.Enabled = "";
    this.upDatedID = "";
    document.getElementById("modal-1").classList.remove("md-show");
  }
  async updateSuperMID(args){
      localStorage.setItem('superMidDetails' , JSON.stringify(args));
      this.router.navigate(['/super-mid/' , 'supermid-edit']);
    
  }
   async deleteRow (args , index) {
    Swal.fire({
      title: 'Are you sure want to remove?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        const itemDeleteId = args.id;
        const obj = {
          "insSuperMIDTable": [
            {
              "id": itemDeleteId,
              "legalname": "",
              "dbaname":   "",
              "address":   "",
              "mobilenumber": "",
              "emailid": "",
              "enabled": "",
              "supermid": ""
            }
          ],
          "operation": "D"
        }
        const response: any = this._apiService.InsertSuperMID(obj);
        
        if (response.status=true) {
          setTimeout (() => {
          
            this.GetSuperMIDDetails();
            Swal.fire(
              response.responseMessage,
              'Success'
            );
         }, 1000);
         
          
        }
       } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelled',
        )
      }
    })

  }

}
