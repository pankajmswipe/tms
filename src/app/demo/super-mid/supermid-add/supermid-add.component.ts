import { Component, OnInit } from '@angular/core';
import { NgbDate, NgbCalendar, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import axios from "axios";
import swal from 'sweetalert2';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/api.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-supermid-add',
  templateUrl: './supermid-add.component.html',
  styleUrls: ['./supermid-add.component.scss']
})
export class SupermidAddComponent implements OnInit {
  Enabled='Y';
  Legal_Name:'';
  DBAName:'';
  Address:'';
  mobile_no:'';
  EmailID:'';
  SuperMID:'';
  constructor( private router: Router,private _apiService: ApiService, private spinner: NgxSpinnerService) { }
  async InsertSuperMID() {
  const obj = {
      "insSuperMIDTable": [
        {
          "id": 0,
          "legalname": this['Legal_Name'],
          "dbaname":   this['DBAName'],
          "address":   this['Address'],
          "mobilenumber": this['mobile_no'],
          "emailid": this['EmailID'],
          "enabled": this['Enabled'],
          "supermid": this['SuperMID']
        }
      ],
      "operation": "I"
    }
    const response: any = await this._apiService.InsertSuperMID(obj);
    if (response.status=true) {
       swal.fire({
        type: 'success',
        text: response.responseMessage
       })
    }else{
      swal.fire({
        type: 'error',
        text: response.responseMessage
       })
    }
  }
  ngOnInit() {
  }
  

}
