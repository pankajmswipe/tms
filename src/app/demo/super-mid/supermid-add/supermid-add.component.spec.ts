import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupermidAddComponent } from './supermid-add.component';

describe('SupermidAddComponent', () => {
  let component: SupermidAddComponent;
  let fixture: ComponentFixture<SupermidAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupermidAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupermidAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
