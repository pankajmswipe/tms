import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupermidViewComponent } from './supermid-view.component';

describe('SupermidViewComponent', () => {
  let component: SupermidViewComponent;
  let fixture: ComponentFixture<SupermidViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupermidViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupermidViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
