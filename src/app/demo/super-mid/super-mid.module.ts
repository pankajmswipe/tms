import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SuperMidRoutes } from './super-mid.routing';
import {SharedModule} from '../../theme/shared/shared.module';
import {DataTablesModule} from 'angular-datatables';
import {NgbDatepickerModule} from '@ng-bootstrap/ng-bootstrap';
import { SupermidAddComponent } from './supermid-add/supermid-add.component';
import { SupermidEditComponent } from './supermid-edit/supermid-edit.component';
import { SupermidListComponent } from './supermid-list/supermid-list.component';
import { SupermidViewComponent } from './supermid-view/supermid-view.component';
import {NgxPaginationModule} from 'ngx-pagination';
@NgModule({
  declarations: [SupermidAddComponent, SupermidEditComponent, SupermidListComponent, SupermidViewComponent],
  imports: [
    CommonModule,
    NgxPaginationModule,
    RouterModule.forChild(SuperMidRoutes),
    SharedModule,
    DataTablesModule,
    NgbDatepickerModule
  ]
})
export class SuperMidModule { }
