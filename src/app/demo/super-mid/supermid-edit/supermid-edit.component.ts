import { Component, OnInit } from '@angular/core';
import { NgbDate, NgbCalendar, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import axios from "axios";
import swal from 'sweetalert2';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/api.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-supermid-edit',
  templateUrl: './supermid-edit.component.html',
  styleUrls: ['./supermid-edit.component.scss']
})
export class SupermidEditComponent implements OnInit {
  Enabled='Y';
  Legal_Name:'';
  DBAName:'';
  Address:'';
  mobile_no:'';
  EmailID:'';
  SuperMID:'';
  upDatedID:'';
  constructor( private router: Router,private _apiService: ApiService, private spinner: NgxSpinnerService) { }


  ngOnInit() {
    const responseData = JSON.parse(localStorage.getItem('superMidDetails'));
      this.Legal_Name = responseData.legalname;
      this.DBAName = responseData.dbaname;
      this.Address = responseData.address;
      this.mobile_no = responseData.mobilenumber;
      this.EmailID = responseData.emailid;
      this.SuperMID = responseData.supermid;
      this.Enabled = responseData.enabled;
      this.upDatedID = responseData.id;
  }
  async updateSuperMID(){
    const obj = {
      "insSuperMIDTable": [
        {
          "id": this.upDatedID,
          "legalname": this['Legal_Name'],
          "dbaname":   this['DBAName'],
          "address":   this['Address'],
          "mobilenumber": this['mobile_no'],
          "emailid": this['EmailID'],
          "enabled": this['Enabled'],
          "supermid": this['SuperMID']
        }
      ],
      "operation": "U"
    }
    const response: any = await this._apiService.InsertSuperMID(obj);
    if (response.status=true) {
       swal.fire({
        type: 'success',
        text: response.responseMessage
       })
    }else{
      swal.fire({
        type: 'error',
        text: response.responseMessage
       })
    }
  }

}
