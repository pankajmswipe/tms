import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupermidEditComponent } from './supermid-edit.component';

describe('SupermidEditComponent', () => {
  let component: SupermidEditComponent;
  let fixture: ComponentFixture<SupermidEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupermidEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupermidEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
