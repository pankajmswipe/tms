import { Routes } from '@angular/router';

import { SupermidListComponent } from './supermid-list/supermid-list.component';
import { SupermidAddComponent } from './supermid-add/supermid-add.component';
import { SupermidViewComponent } from './supermid-view/supermid-view.component';
import { SupermidEditComponent } from './supermid-edit/supermid-edit.component';

export const SuperMidRoutes: Routes = [
    {

      path: '',
      children: [ {
        path: 'supermid-list',
        component: SupermidListComponent
    },{
        path: 'supermid-add',
        component: SupermidAddComponent
    },{
        path: 'supermid-edit',
        component: SupermidEditComponent
    },{
        path: 'supermid-view',
        component: SupermidViewComponent
    }]
}
];