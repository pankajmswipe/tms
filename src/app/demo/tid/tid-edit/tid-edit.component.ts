import { Component, OnInit } from '@angular/core';
import { NgbDate, NgbCalendar, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import axios from "axios";
import swal from 'sweetalert2';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/api.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-tid-edit',
  templateUrl: './tid-edit.component.html',
  styleUrls: ['./tid-edit.component.scss']
})
export class TidEditComponent implements OnInit {

  TID:'';
  MID='MID000000000001';
  Enabled='Y';
  SerialNumber:'';
  TMK:'';
  TMKKCV:'';
  TAK:'';
  TAKKCV:'';
  KeySrnr:'';
  ProfileName='Retail';
  TPKKCV:'';
  TPK:'';
 constructor( private router: Router,private _apiService: ApiService, private spinner: NgxSpinnerService) { }
  ngOnInit() {
    const responseData = JSON.parse(localStorage.getItem('tidDetails'));
    this.TID=responseData.tid;
    this.MID=responseData.mid;
    this.Enabled=responseData.enabled;
    this.SerialNumber=responseData.serialnumber;
    this.TMK=responseData.tmk;
    this.TMKKCV=responseData.tmk_kcv;
    this.TAK=responseData.tak;
    this.TAKKCV=responseData.tak_kcv;
    this.KeySrnr=responseData.keysrnr;
    this.ProfileName=responseData.profilename;
  }
  async InsertTIDDetails(){
    const obj = {
      "insTMSTIDTable": [
        {
          "tid": this['TID'],
          "mid": this['MID'],
          "enabled": this['Enabled'],
          "serialnumber": this['SerialNumber'],
          "tmk": this['TMK'],
          "tmk_kcv": this['TMKKCV'],
          "tpk": this['TPK'],
          "tpk_kcv": this['TPKKCV'],
          "tak": this['TAK'],
          "tak_kcv": this['TAKKCV'],
          "keysrnr": this['KeySrnr'],
          "profilename": this['ProfileName']
        }
      ],
        "operation": "U"
      }
    const response: any = await this._apiService.InsertTIDDetails(obj);
    if (response.status=true) {
       swal.fire({
        type: 'success',
        text: response.responseMessage
       })
    }else{
      swal.fire({
        type: 'error',
        text: response.responseMessage
       })
    }
  }

}

