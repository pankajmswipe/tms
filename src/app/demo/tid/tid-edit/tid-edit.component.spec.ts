import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TidEditComponent } from './tid-edit.component';

describe('TidEditComponent', () => {
  let component: TidEditComponent;
  let fixture: ComponentFixture<TidEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TidEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TidEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
