import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TidViewComponent } from './tid-view.component';

describe('TidViewComponent', () => {
  let component: TidViewComponent;
  let fixture: ComponentFixture<TidViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TidViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TidViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
