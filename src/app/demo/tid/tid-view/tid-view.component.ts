import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tid-view',
  templateUrl: './tid-view.component.html',
  styleUrls: ['./tid-view.component.scss']
})
export class TidViewComponent implements OnInit {
  TID:'';
  MID='MID000000000001';
  Enabled='Y';
  SerialNumber:'';
  TMK:'';
  TMKKCV:'';
  TAK:'';
  TAKKCV:'';
  KeySrnr:'';
  ProfileName='Retail';
  TPKKCV:'';
  TPK:'';
  constructor() { }

  ngOnInit() {
  }

}
