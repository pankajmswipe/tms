import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TidRoutes } from './tid-routing';
import {SharedModule} from '../../theme/shared/shared.module';
import {DataTablesModule} from 'angular-datatables';
import {NgbDatepickerModule} from '@ng-bootstrap/ng-bootstrap';
import { TidViewComponent } from './tid-view/tid-view.component';
import { TidEditComponent } from './tid-edit/tid-edit.component';
import { TidListComponent } from './tid-list/tid-list.component';
import { TidAddComponent } from './tid-add/tid-add.component';
import {NgxPaginationModule} from 'ngx-pagination';
@NgModule({
  declarations: [TidViewComponent, TidEditComponent, TidListComponent, TidAddComponent],
  imports: [
    CommonModule,
    NgxPaginationModule,
    RouterModule.forChild(TidRoutes),
    SharedModule,
    DataTablesModule,
    NgbDatepickerModule
  ]
})
export class TidModule { }