import { Routes } from '@angular/router';

import { TidListComponent } from './tid-list/tid-list.component';
import { TidAddComponent } from './tid-add/tid-add.component';
import { TidViewComponent } from './tid-view/tid-view.component';
import { TidEditComponent } from './tid-edit/tid-edit.component';

export const TidRoutes: Routes = [
    {

      path: '',
      children: [ {
        path: 'tid-list',
        component: TidListComponent
    },{
        path: 'tid-add',
        component: TidAddComponent
    },{
        path: 'tid-edit',
        component: TidEditComponent
    },{
        path: 'tid-view',
        component: TidViewComponent
    }]
}
];