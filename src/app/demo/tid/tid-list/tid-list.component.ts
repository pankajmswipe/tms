import { Component, OnInit } from '@angular/core';
import { NgbDate, NgbCalendar, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import axios from "axios";
import swal from 'sweetalert2';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tid-list',
  templateUrl: './tid-list.component.html',
  styleUrls: ['./tid-list.component.scss']
})
export class TidListComponent implements OnInit {

  appsTID;
  getTIDDetails:any=[];
  TID:'';
  MID:'Y';
  Enabled:'Y';
  SerialNumber:'';
  TMK:'';
  TMKKCV:'';
  TAK:'';
  TAKKCV:'';
  KeySrnr:'';
  ProfileName:'';

  constructor( private router: Router,private _apiService: ApiService, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.GetTIDDetails();
  }
  async GetTIDDetails() {
    const response: any = await this._apiService.GetTIDDetails();
    if (response) {
      this.getTIDDetails = response.table;
    }
  }
  addTID(){
    this.router.navigate(["/tid/tid-add"]);
  }
  async viewTid(args){
    //console.log(args);
    this.TID=args.tid;
    this.MID=args.mid;
    this.Enabled=args.enabled;
    this.SerialNumber=args.serialnumber;
    this.TMK=args.tmk;
    this.TMKKCV=args.tmk_kcv;
    this.TAK=args.tak;
    this.TAKKCV=args.tak_kcv;
    this.KeySrnr=args.keysrnr;
    this.ProfileName=args.profilename;
  document.querySelector('#modal-3').classList.add('md-show');
}
closePopUpID(){
  this.TID='';
  this.MID='Y';
  this.Enabled='Y';
  this.SerialNumber='';
  this.TMK='';
  this.TMKKCV='';
  this.TAK='';
  this.TAKKCV='';
  this.KeySrnr='';
  this.ProfileName='';
  document.getElementById("modal-3").classList.remove("md-show");
}
async updateMid(args){
    localStorage.setItem('tidDetails' , JSON.stringify(args));
    //console.log(args);
    this.router.navigate(['/tid' , 'tid-edit']);
  
}
 async deleteTid (args , index) {
  Swal.fire({
    title: 'Are you sure want to remove?',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes, delete it!',
    cancelButtonText: 'No, keep it'
  }).then((result) => {
    if (result.value) {
      const itemDeleteId = args.tid;
      const obj = {
        "insTMSTIDTable": [
          {
            "tid": itemDeleteId,//this['TID'],
            "mid": '',
            "enabled": '',
            "serialnumber": '',
            "tmk": '',
            "tmk_kcv": '',
            "tpk": '',
            "tpk_kcv": '',
            "tak": '',
            "tak_kcv": '',
            "keysrnr": '',
            "profilename": ''
          }
        ],
        "operation": "D"
      }
      const response: any = this._apiService.InsertTIDDetails(obj);
      
      if (response.status=true) {
        setTimeout (() => {
        
          this.GetTIDDetails();
          Swal.fire(
            response.responseMessage,
            'Success'
          );
       }, 1000);
       
        
      }
     } else if (result.dismiss === Swal.DismissReason.cancel) {
      Swal.fire(
        'Cancelled',
      )
    }
  })
}


}
