import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TidListComponent } from './tid-list.component';

describe('TidListComponent', () => {
  let component: TidListComponent;
  let fixture: ComponentFixture<TidListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TidListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TidListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
