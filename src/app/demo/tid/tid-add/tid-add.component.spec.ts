import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TidAddComponent } from './tid-add.component';

describe('TidAddComponent', () => {
  let component: TidAddComponent;
  let fixture: ComponentFixture<TidAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TidAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TidAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
