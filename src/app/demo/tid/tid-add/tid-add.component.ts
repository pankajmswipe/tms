import { Component, OnInit } from '@angular/core';
import { NgbDate, NgbCalendar, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import axios from "axios";
import swal from 'sweetalert2';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/api.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-tid-add',
  templateUrl: './tid-add.component.html',
  styleUrls: ['./tid-add.component.scss']
})
export class TidAddComponent implements OnInit {

  TID:'';
  MID='MID000000000001';
  Enabled='Y';
  SerialNumber:'';
  TMK:'';
  TMKKCV:'';
  TAK:'';
  TAKKCV:'';
  KeySrnr:'';
  ProfileName='Retail';
  TPKKCV:'';
  TPK:'';
  constructor( private router: Router,private _apiService: ApiService, private spinner: NgxSpinnerService) { }
  async InsertTIDDetails() {
    const obj = {
      "insTMSTIDTable": [
        {
          "tid": this['TID'],
          "mid": this['MID'],
          "enabled": this['Enabled'],
          "serialnumber": this['SerialNumber'],
          "tmk": this['TMK'],
          "tmk_kcv": this['TMKKCV'],
          "tpk": this['TPK'],
          "tpk_kcv": this['TPKKCV'],
          "tak": this['TAK'],
          "tak_kcv": this['TAKKCV'],
          "keysrnr": this['KeySrnr'],
          "profilename": this['ProfileName']
        }
      ],
      "operation": "I"
    }
    const response: any = await this._apiService.InsertTIDDetails(obj);
    if (response.status=true) {
       swal.fire({
        type: 'success',
        text: response.responseMessage
       })
    }else{
      swal.fire({
        type: 'error',
        text: response.responseMessage
       })
    }
  }
  ngOnInit() {
  }

}
