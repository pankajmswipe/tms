import { ApiService } from 'src/app/api.service';
import { Router } from "@angular/router";
import { NgForm } from "@angular/forms";
import { Component, OnInit, ElementRef, OnDestroy } from "@angular/core";
import axios from "axios";
import swal from "sweetalert2";
import { loginURL } from './../../../../global';

@Component({
  selector: "app-auth-signin-v4",
  templateUrl: "./auth-signin-v4.component.html",
  styleUrls: [
    "./auth-signin-v4.component.scss",
    "../../../../../scss/theme-elements/_auth-2.scss"
  ]
})
export class AuthSigninV4Component implements OnInit {
  username: any;
  password: any;
  domainIP;
  isAuthorized: boolean= false;
  systemdomainIP:any=[];
  axios = this._axiosService.axios;
  isPasswordVisible: boolean = false;
  constructor(
    private element: ElementRef,
    private route: Router,
    private _axiosService: ApiService
  ) {}
  togglePasswordVisibility() {
    this.isPasswordVisible = !this.isPasswordVisible;
  }
  
  // async onSubmit(form: NgForm) {
  //   // this.route.navigate(['dashboard']);
  //   if (form.valid) {
  //     // const response: any = await this.axios.post('/login', {
  //     const response: any = await this.axios.post(loginURL + "/ERPLogin", {
  //       username: form.value.username,
  //       password: form.value.password,
  //       pc_name: "df",
  //       macId: "234"
  //     });
  //     if (response) {
  //       if (response.responseCode == 200) {
         
  //         localStorage.setItem("isLoggedIn", "true");
  //         localStorage.setItem("agent_mobile",response.stationId);
  //         localStorage.setItem("agentUsername",form.value.username);
  //         setTimeout(() => {
  //           this.route.navigate(["dashboard/help-desk"]);
  //          }, 3000);
        
         
  //       } else {
  //         swal.fire({
  //           type: "error",
  //           title: "Invalid",
  //           text: response.responseMessage
  //         });
  //       }
  //     } else {
  //       swal.fire({
  //         type: "error",
  //         title: "Invalid",
  //         text: "Oops something went wrong!"
  //       });
  //     }
  //   }else{
  //      swal.fire({
  //       type: "error",
  //       title: "Invalid",
  //       text: "Please provide username/password."
  //     });

  //   }
  // }
  async loginSubmit() {
    if (this['username'] && this['password']) {
      const response: any = await this.axios.post(loginURL + "/ERPLogin",{
        "username": this['username'],
        "password": this['password'],
      });
      if (response) {
        console.log(response);
        if (response.responseCode == 200) {
              localStorage.setItem("isLoggedIn", "true");
              localStorage.setItem("agent_mobile",response.stationId);
              localStorage.setItem("agentUsername",this['username']);
              setTimeout(() => {
              this.route.navigate(["dashboard/help-desk"]);
              }, 3000);
                
        }else{
          swal.fire({
            type: "error",
            title: "Invalid",
            text: "Incorrect Username/Password"
        });
        }
      }
    } else {
      swal.fire({
          type: "error",
          title: "Invalid",
          text: "Invalid username or password"
      });
    }
  }

  ngOnInit() {
  

  }
}

