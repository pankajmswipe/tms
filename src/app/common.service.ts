import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  applicationNumber: any = new Subject();
  constructor() {}
  setApplicationNumber(args) {
    this.applicationNumber.next(args);
  }
}
