import { Injectable } from '@angular/core';
import { AxiosService } from './axios.service';
//import {erpUrl} from 'src/app/global';
//import {adhaarUrl} from 'src/app/global';
@Injectable({
  providedIn: 'root'
})
export class ApiService {
  axios = this._axiosService.axios;
  
  constructor(private _axiosService: AxiosService) { }

  async GetSuperMIDDetails(){
    return await this.axios.get( '/GetSuperMIDDetails');
  
  }
  async GetMIDDetails(){
    return await this.axios.get( '/GetTMSMID');
  }
  async GetTIDDetails(){
    return await this.axios.get( '/GetTMSTID');
  }
  async GetTMSProfileDetails(){
    return await this.axios.get( '/GetTMSProfileDetails');
  }
  async InsertSuperMID(payload: object){
      return await this.axios.post('/InsertSuperMID', payload);
  }
  async InsertMIDDetails(payload: object){
    return await this.axios.post('/InsertMIDDetails', payload);
  }
  async InsertTIDDetails(payload: object){
    return await this.axios.post('/InsertTIDDetails', payload);
  }
  async InsertTMSProfileDetails(payload: object){
    return await this.axios.post('/InsertTMSProfileDetails', payload);
  }
  
  
}
