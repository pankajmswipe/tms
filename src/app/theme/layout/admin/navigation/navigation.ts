import { Injectable } from '@angular/core';

export interface NavigationItem {
  id: string;
  title: string;
  type: 'item' | 'collapse' | 'group';
  translate?: string;
  icon?: string;
  hidden?: boolean;
  url?: string;
  classes?: string;
  exactMatch?: boolean;
  external?: boolean;
  target?: boolean;
  breadcrumbs?: boolean;
  function?: any;
  badge?: {
    title?: string;
    type?: string;
  };
  children?: Navigation[];

}

export interface Navigation extends NavigationItem {
  children?: NavigationItem[];
}



const NavigationItems = [
  {
    id: 'navigation',
    title: 'Navigation',
    type: 'group',
    icon: 'feather icon-monitor',
    children: [
      {
        id: 'Super-MID',
        title: 'Super MID',
        type: 'item',
        url: '/super-mid/supermid-list',
        icon: 'feather icon-settings',
        children: [
          {
            id: 'Super-MID',
            title: 'Super MID',
            type: 'item',
            url: '/super-mid/supermid-list',
          }
        ]
      },
      {
        id: 'MID',
        title: 'MID',
        type: 'item',
        url: '/mid/mid-list',
        icon: 'feather icon-settings',
        children: [
          {
            id: 'MID',
            title: 'MID',
            type: 'item',
            url: '/mid/mid-list',
          }
        ]
      },
      {
        id: 'TID',
        title: 'TID',
        type: 'item',
        url: '/tid/tid-list',
        icon: 'feather icon-settings',
        children: [
          {
            id: 'TID',
            title: 'TID',
            type: 'item',
            url: '/tid/tid-list',
          }
        ]
      },
    ]
  },
  
];

const NavigationItemsTwo = [
  {
    id: 'navigation',
    title: 'Navigation',
    type: 'group',
    icon: 'feather icon-monitor',
    children: [
      {
        id: 'Profile',
        title: 'Profile',
        type: 'item',
        url: '/profile/profile-list',
        icon: 'feather icon-user',
        children: [
          {
            id: 'Profile',
            title: 'Profile',
            type: 'item',
            url: '/profile/profile-list',
          }
        ]
      },
     
    ]
  },
  
];

@Injectable()
export class NavigationItem {
  activeAgent;
  get() {
    this.activeAgent = localStorage.getItem("agentUsername");
    console.log(this.activeAgent);
    if (this.activeAgent === 'TANVI') {
      return NavigationItems;
    } else {
      return NavigationItemsTwo;
    }
  }
}
