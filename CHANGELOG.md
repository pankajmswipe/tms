# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.1-beta.37](https://gitlab.com/mswipe/mob-2.0/compare/v0.0.1-beta.35...v0.0.1-beta.37) (2020-08-21)


### Features

* **MOB-FORM:** add final save and discripancy api ([8c4249f](https://gitlab.com/mswipe/mob-2.0/commit/8c4249f9c759d3f4c403244b29548ecc511fdc74))
* **MOB-FORM:** add final save and discripancy api ([dc46c37](https://gitlab.com/mswipe/mob-2.0/commit/dc46c371c8ccecc9dd73a051df7fa60418b49384))
* **Product commercials, MOB, Process menu:** Payment data fetching to main component, Terminal Array creation with dummy data. ([62c4f21](https://gitlab.com/mswipe/mob-2.0/commit/62c4f21f80dd405976112346067786e81a73f105))

### [0.0.1-beta.36](https://gitlab.com/mswipe/mob-2.0/compare/v0.0.1-beta.34...v0.0.1-beta.36) (2020-08-20)


### Features

* **MOB:** Datable plotting changes ([542c2a4](https://gitlab.com/mswipe/mob-2.0/commit/542c2a4250ee8a40ab621430838a8825dbf135fc))
* **MOB-FORM:** add final save and discripancy api ([8c4249f](https://gitlab.com/mswipe/mob-2.0/commit/8c4249f9c759d3f4c403244b29548ecc511fdc74))
* **MOB-FORM:** add final save and discripancy api ([dc46c37](https://gitlab.com/mswipe/mob-2.0/commit/dc46c371c8ccecc9dd73a051df7fa60418b49384))

### [0.0.1-beta.35](https://gitlab.com/mswipe/mob-2.0/compare/v0.0.1-beta.32...v0.0.1-beta.35) (2020-08-19)


### Features

* **MOB:** Datable plotting changes ([542c2a4](https://gitlab.com/mswipe/mob-2.0/commit/542c2a4250ee8a40ab621430838a8825dbf135fc))
* **MOB-FORM:** add final save and discripancy api ([f082d1d](https://gitlab.com/mswipe/mob-2.0/commit/f082d1da88cf32021dc44daa60d158f15b056786))
* **MOB-FORM:** add final save api and databind ([c57363f](https://gitlab.com/mswipe/mob-2.0/commit/c57363f877db009259771541f7222a43968f1627))
* **MOB-FORM:** add final save api and databind ([8317149](https://gitlab.com/mswipe/mob-2.0/commit/831714931a07988ca8035b84e86503510e9c8fb2))

### [0.0.1-beta.34](https://gitlab.com/mswipe/mob-2.0/compare/v0.0.1-beta.33...v0.0.1-beta.34) (2020-08-19)


### Features

* **MOB-FORM:** add final save and discripancy api ([f082d1d](https://gitlab.com/mswipe/mob-2.0/commit/f082d1da88cf32021dc44daa60d158f15b056786))

### [0.0.1-beta.33](https://gitlab.com/mswipe/mob-2.0/compare/v0.0.1-beta.31...v0.0.1-beta.33) (2020-08-18)


### Features

* **MOB-FORM:** add final save api and databind ([c57363f](https://gitlab.com/mswipe/mob-2.0/commit/c57363f877db009259771541f7222a43968f1627))
* **MOB-FORM:** add final save api and databind ([8317149](https://gitlab.com/mswipe/mob-2.0/commit/831714931a07988ca8035b84e86503510e9c8fb2))
* **MOB, Process Menu:** On Application Seacrch MOb tab not fill if i switch anthother tab again go on mob tab then fill, In Mob Tab  Add Discepancy screen below click on save need dropdown label in request parameter, how to pass appno global so that we can pass in any where in request parameter, Outpit emit from child component ([dcc58a0](https://gitlab.com/mswipe/mob-2.0/commit/dcc58a086272e13ff6c7f70a93b8c2edf19c01c4))

### [0.0.1-beta.32](https://gitlab.com/mswipe/mob-2.0/compare/v0.0.1-beta.25...v0.0.1-beta.32) (2020-08-18)


### Features

* **MOB-FORM:** add missing fields bind ([86a4136](https://gitlab.com/mswipe/mob-2.0/commit/86a4136f566dbbe4dcf2ee6d1bdf8813b2f6c26b))
* **MOB-FORM:** add ngMOdelChange ([d378559](https://gitlab.com/mswipe/mob-2.0/commit/d378559166289a04add26e465ec15d150442e649))
* **MOB-FORM:** add payment screen and login with login api ([4742227](https://gitlab.com/mswipe/mob-2.0/commit/4742227948a1d1297430252b4520415254a3d643))
* **MOB-FORM:** add payment screen data bind and device details databind ([df2f667](https://gitlab.com/mswipe/mob-2.0/commit/df2f667609d0e649054defd6782276e4d2a1d847))
* **MOB-FORM:** add payment screen data bind and device details databind ([8745589](https://gitlab.com/mswipe/mob-2.0/commit/8745589b2c3507c6336e3f97e2eb8c4bd3397896))
* **MOB-FORM:** kyc databinding and object model binding ([fcff85b](https://gitlab.com/mswipe/mob-2.0/commit/fcff85ba645445d5fa6984819c2b66a01db23d13))
* **MOB, Process Menu:** On Application Seacrch MOb tab not fill if i switch anthother tab again go on mob tab then fill, In Mob Tab  Add Discepancy screen below click on save need dropdown label in request parameter, how to pass appno global so that we can pass in any where in request parameter, Outpit emit from child component ([dcc58a0](https://gitlab.com/mswipe/mob-2.0/commit/dcc58a086272e13ff6c7f70a93b8c2edf19c01c4))

### [0.0.1-beta.31](https://gitlab.com/mswipe/mob-2.0/compare/v0.0.1-beta.30...v0.0.1-beta.31) (2020-08-18)


### Features

* **MOB-FORM:** add missing fields bind ([86a4136](https://gitlab.com/mswipe/mob-2.0/commit/86a4136f566dbbe4dcf2ee6d1bdf8813b2f6c26b))

### [0.0.1-beta.30](https://gitlab.com/mswipe/mob-2.0/compare/v0.0.1-beta.29...v0.0.1-beta.30) (2020-08-18)


### Features

* **MOB-FORM:** add ngMOdelChange ([d378559](https://gitlab.com/mswipe/mob-2.0/commit/d378559166289a04add26e465ec15d150442e649))

### [0.0.1-beta.29](https://gitlab.com/mswipe/mob-2.0/compare/v0.0.1-beta.28...v0.0.1-beta.29) (2020-08-17)


### Features

* **MOB-FORM:** add payment screen data bind and device details databind ([df2f667](https://gitlab.com/mswipe/mob-2.0/commit/df2f667609d0e649054defd6782276e4d2a1d847))

### [0.0.1-beta.28](https://gitlab.com/mswipe/mob-2.0/compare/v0.0.1-beta.27...v0.0.1-beta.28) (2020-08-17)


### Features

* **MOB-FORM:** add payment screen data bind and device details databind ([8745589](https://gitlab.com/mswipe/mob-2.0/commit/8745589b2c3507c6336e3f97e2eb8c4bd3397896))

### [0.0.1-beta.27](https://gitlab.com/mswipe/mob-2.0/compare/v0.0.1-beta.26...v0.0.1-beta.27) (2020-08-16)


### Features

* **MOB-FORM:** add payment screen and login with login api ([4742227](https://gitlab.com/mswipe/mob-2.0/commit/4742227948a1d1297430252b4520415254a3d643))

### [0.0.1-beta.26](https://gitlab.com/mswipe/mob-2.0/compare/v0.0.1-beta.23...v0.0.1-beta.26) (2020-08-14)


### Features

* **MOB:** Conflict resolved ([88ff9f8](https://gitlab.com/mswipe/mob-2.0/commit/88ff9f8d4896d9a4b0c7db50a7b43418d4a8f6be))
* **MOB:** Image slider implementation with static response, Existing Image slider removed ([59752ca](https://gitlab.com/mswipe/mob-2.0/commit/59752cad90ea9cb1fd2a8e3dc4920af3102c7a62))
* **MOB , PRoduct commercials:**  MOB Variabled binding object created, Product commercial response conditions added ([1be01f7](https://gitlab.com/mswipe/mob-2.0/commit/1be01f7a737e010608ae6e5809506c0f9c8524a1))
* **MOB-FORM:** kyc databinding and object model binding ([fcff85b](https://gitlab.com/mswipe/mob-2.0/commit/fcff85ba645445d5fa6984819c2b66a01db23d13))


### Bug Fixes

* **MOB:** Conflict resolved ([a7231a4](https://gitlab.com/mswipe/mob-2.0/commit/a7231a4589693aa9888981537d0432025860c433))
* **MOB:** Conflicts fixed ([51c8882](https://gitlab.com/mswipe/mob-2.0/commit/51c88828fe045d98b193b145ddc3978f5caadb77))

### [0.0.1-beta.25](https://gitlab.com/mswipe/mob-2.0/compare/v0.0.1-beta.24...v0.0.1-beta.25) (2020-08-13)


### Features

* **MOB:** Conflict resolved ([88ff9f8](https://gitlab.com/mswipe/mob-2.0/commit/88ff9f8d4896d9a4b0c7db50a7b43418d4a8f6be))

### [0.0.1-beta.24](https://gitlab.com/mswipe/mob-2.0/compare/v0.0.1-beta.18...v0.0.1-beta.24) (2020-08-13)


### Features

* **MOB:** Image slider implementation with static response, Existing Image slider removed ([59752ca](https://gitlab.com/mswipe/mob-2.0/commit/59752cad90ea9cb1fd2a8e3dc4920af3102c7a62))
* **MOB , PRoduct commercials:**  MOB Variabled binding object created, Product commercial response conditions added ([1be01f7](https://gitlab.com/mswipe/mob-2.0/commit/1be01f7a737e010608ae6e5809506c0f9c8524a1))
* **MOB-FORM:** api dedupe api and payment details api ([6b27ecf](https://gitlab.com/mswipe/mob-2.0/commit/6b27ecfcaebff799d5363cd831d2c95f49889b81))
* **MOB-FORM:** api integration and screen design ([dd303f0](https://gitlab.com/mswipe/mob-2.0/commit/dd303f0cbce1a33ce7bdbc90291ac1ee6fdb2835))
* **MOB-FORM:** api integration image and dynamic parameter pass ([39fff66](https://gitlab.com/mswipe/mob-2.0/commit/39fff6647c16d6369af7775ecf42e74c3adadeb2))
* **MOB-FORM:** api integration of mob tabs ([e2211fb](https://gitlab.com/mswipe/mob-2.0/commit/e2211fb2bbab202c18c983720cc3499f91c9b1e1))
* **MOB-FORM:** api integration of mob tabs ([c9f2da6](https://gitlab.com/mswipe/mob-2.0/commit/c9f2da65c0561208486e763b084f50c9732cb087))


### Bug Fixes

* **MOB:** Conflict resolved ([a7231a4](https://gitlab.com/mswipe/mob-2.0/commit/a7231a4589693aa9888981537d0432025860c433))
* **MOB:** Conflicts fixed ([51c8882](https://gitlab.com/mswipe/mob-2.0/commit/51c88828fe045d98b193b145ddc3978f5caadb77))

### [0.0.1-beta.23](https://gitlab.com/mswipe/mob-2.0/compare/v0.0.1-beta.22...v0.0.1-beta.23) (2020-08-13)


### Features

* **MOB-FORM:** api integration and screen design ([dd303f0](https://gitlab.com/mswipe/mob-2.0/commit/dd303f0cbce1a33ce7bdbc90291ac1ee6fdb2835))

### [0.0.1-beta.22](https://gitlab.com/mswipe/mob-2.0/compare/v0.0.1-beta.21...v0.0.1-beta.22) (2020-08-12)


### Features

* **MOB-FORM:** api integration image and dynamic parameter pass ([39fff66](https://gitlab.com/mswipe/mob-2.0/commit/39fff6647c16d6369af7775ecf42e74c3adadeb2))

### [0.0.1-beta.21](https://gitlab.com/mswipe/mob-2.0/compare/v0.0.1-beta.20...v0.0.1-beta.21) (2020-08-11)


### Features

* **MOB-FORM:** api integration of mob tabs ([e2211fb](https://gitlab.com/mswipe/mob-2.0/commit/e2211fb2bbab202c18c983720cc3499f91c9b1e1))

### [0.0.1-beta.20](https://gitlab.com/mswipe/mob-2.0/compare/v0.0.1-beta.19...v0.0.1-beta.20) (2020-08-11)


### Features

* **MOB-FORM:** api integration of mob tabs ([c9f2da6](https://gitlab.com/mswipe/mob-2.0/commit/c9f2da65c0561208486e763b084f50c9732cb087))

### [0.0.1-beta.19](https://gitlab.com/mswipe/mob-2.0/compare/v0.0.1-beta.18...v0.0.1-beta.19) (2020-08-10)


### Features

* **MOB-FORM:** api dedupe api and payment details api ([6b27ecf](https://gitlab.com/mswipe/mob-2.0/commit/6b27ecfcaebff799d5363cd831d2c95f49889b81))

### [0.0.1-beta.18](https://gitlab.com/mswipe/mob-2.0/compare/v0.0.1-beta.17...v0.0.1-beta.18) (2020-08-10)

### [0.0.1-beta.17](https://gitlab.com/mswipe/mob-2.0/compare/v0.0.1-beta.15...v0.0.1-beta.17) (2020-07-14)


### Features

* **MOB-FORM:** api integration form ([94d33bb](https://gitlab.com/mswipe/mob-2.0/commit/94d33bb65b2fba02e4c49b630032e83240e9e63d))

### [0.0.1-beta.16](https://gitlab.com/mswipe/mob-2.0/compare/v0.0.1-beta.11...v0.0.1-beta.16) (2020-07-14)


### Features

* **Axios, API wrapper:** API wrapper and axios wrapper created, spinner added ([f30b454](https://gitlab.com/mswipe/mob-2.0/commit/f30b45420c53250fa4551841f3782115e263f61f))
* **MOB:** KYC Preview changes ([3318f55](https://gitlab.com/mswipe/mob-2.0/commit/3318f55d8ad4dd8d12b6ba6edcb763889d820d2e))
* **MOB-FORM:** api integration form ([94d33bb](https://gitlab.com/mswipe/mob-2.0/commit/94d33bb65b2fba02e4c49b630032e83240e9e63d))
* **Payment:** Responsive changes ([28762ce](https://gitlab.com/mswipe/mob-2.0/commit/28762ce2480edd4f0854110fe0280085387bf424))
* **Responsive, Header :** responsive changes , header logo added ([bd6c3ca](https://gitlab.com/mswipe/mob-2.0/commit/bd6c3ca43fd86e8ee405bf0192b461c505aa70a8))

### [0.0.1-beta.15](https://gitlab.com/mswipe/mob-2.0/compare/v0.0.1-beta.14...v0.0.1-beta.15) (2020-07-14)


### Features

* **Axios, API wrapper:** API wrapper and axios wrapper created, spinner added ([f30b454](https://gitlab.com/mswipe/mob-2.0/commit/f30b45420c53250fa4551841f3782115e263f61f))

### [0.0.1-beta.14](https://gitlab.com/mswipe/mob-2.0/compare/v0.0.1-beta.13...v0.0.1-beta.14) (2020-07-14)


### Features

* **Payment:** Responsive changes ([28762ce](https://gitlab.com/mswipe/mob-2.0/commit/28762ce2480edd4f0854110fe0280085387bf424))

### [0.0.1-beta.13](https://gitlab.com/mswipe/mob-2.0/compare/v0.0.1-beta.12...v0.0.1-beta.13) (2020-07-09)


### Features

* **Responsive, Header :** responsive changes , header logo added ([bd6c3ca](https://gitlab.com/mswipe/mob-2.0/commit/bd6c3ca43fd86e8ee405bf0192b461c505aa70a8))

### [0.0.1-beta.12](https://gitlab.com/mswipe/mob-2.0/compare/v0.0.1-beta.10...v0.0.1-beta.12) (2020-07-08)


### Features

* **MOB:** KYC Preview changes ([3318f55](https://gitlab.com/mswipe/mob-2.0/commit/3318f55d8ad4dd8d12b6ba6edcb763889d820d2e))
* **MOB-FORM:** payments tab screen design ([5789ac2](https://gitlab.com/mswipe/mob-2.0/commit/5789ac2cb8e334064d4a68faa9742839650c152f))

### [0.0.1-beta.11](https://gitlab.com/mswipe/mob-2.0/compare/v0.0.1-beta.9...v0.0.1-beta.11) (2020-07-06)


### Features

* **MOB:** KYC BANK section UI changes ([e262f7d](https://gitlab.com/mswipe/mob-2.0/commit/e262f7dc3c934cecdc4f204d370eea3a27346c97))
* **MOB-FORM:** payments tab screen design ([5789ac2](https://gitlab.com/mswipe/mob-2.0/commit/5789ac2cb8e334064d4a68faa9742839650c152f))

### [0.0.1-beta.10](https://gitlab.com/mswipe/mob-2.0/compare/v0.0.1-beta.8...v0.0.1-beta.10) (2020-07-04)


### Features

* **MOB:** KYC BANK section UI changes ([e262f7d](https://gitlab.com/mswipe/mob-2.0/commit/e262f7dc3c934cecdc4f204d370eea3a27346c97))
* **MOB-FORM:** add discripancy screen aff ([f673e75](https://gitlab.com/mswipe/mob-2.0/commit/f673e75dadb77debdbbc51f3fd4f13d0e58fa585))

### [0.0.1-beta.9](https://gitlab.com/mswipe/mob-2.0/compare/v0.0.1-beta.7...v0.0.1-beta.9) (2020-07-03)


### Features

* **MOB:** merchant details, Login details, Registered Office Address , Residentials Address, Installation address UI complete. ([23c747c](https://gitlab.com/mswipe/mob-2.0/commit/23c747c14ba80d086ae5ef1afbe1295ce29530b2))
* **MOB-FORM:** add discripancy screen aff ([f673e75](https://gitlab.com/mswipe/mob-2.0/commit/f673e75dadb77debdbbc51f3fd4f13d0e58fa585))


### Bug Fixes

* **Appl Process module:** Conflict resolved ([de3d309](https://gitlab.com/mswipe/mob-2.0/commit/de3d309ecf2c9ae17310b5261f79922edee2afaf))

### [0.0.1-beta.8](https://gitlab.com/mswipe/mob-2.0/compare/v0.0.1-beta.7...v0.0.1-beta.8) (2020-07-02)


### Features

* **MOB-FORM:** product and commercial details screen design ([df892c2](https://gitlab.com/mswipe/mob-2.0/commit/df892c2fac86cb1c515b77f0f7338f21a76bec8e))


### Bug Fixes

* **Appl Process module:** Conflict resolved ([de3d309](https://gitlab.com/mswipe/mob-2.0/commit/de3d309ecf2c9ae17310b5261f79922edee2afaf))

### [0.0.1-beta.7](https://gitlab.com/mswipe/mob-2.0/compare/v0.0.1-beta.5...v0.0.1-beta.7) (2020-07-02)


### Features

* **MOB:** Login details fields UI development. ([5b3c00a](https://gitlab.com/mswipe/mob-2.0/commit/5b3c00a4ab87e1ee4c0e74ee0b78b29f9d0a69e3))
* **MOB-FORM:** product and commercial details screen design ([df892c2](https://gitlab.com/mswipe/mob-2.0/commit/df892c2fac86cb1c515b77f0f7338f21a76bec8e))

### [0.0.1-beta.6](https://gitlab.com/mswipe/mob-2.0/compare/v0.0.1-beta.4...v0.0.1-beta.6) (2020-07-01)


### Features

* **MOB:** Login details fields UI development. ([5b3c00a](https://gitlab.com/mswipe/mob-2.0/commit/5b3c00a4ab87e1ee4c0e74ee0b78b29f9d0a69e3))
* **MOB-FORM:** conflict resolved ([54718ed](https://gitlab.com/mswipe/mob-2.0/commit/54718ed36aa53aff1a01ec97e3fc0d5d77b8e667))
* **MOB-FORM:** de-dupe form design and conflict resolved ([9853ac6](https://gitlab.com/mswipe/mob-2.0/commit/9853ac69efa240f0b3f70514db8e28c2099a900e))
* **MOB-FORM:** discrepancy grid design ([bb98299](https://gitlab.com/mswipe/mob-2.0/commit/bb982995c892bae79bf50cfa2f85da399b29b558))
* **MOB-FORM:** discrepancy grid design ([07f97d3](https://gitlab.com/mswipe/mob-2.0/commit/07f97d346305fc5f353403ec996ce2bd11b6ae4f))

### [0.0.1-beta.5](https://gitlab.com/mswipe/mob-2.0/compare/v0.0.1-beta.1...v0.0.1-beta.5) (2020-07-01)


### Features

* **Application Process  Module:** Module , routing and component created MOB development started ([db425ed](https://gitlab.com/mswipe/mob-2.0/commit/db425edf72f333dae5753af5190f1a92875cbd1c))
* **MOB-FORM:** conflict resolved ([54718ed](https://gitlab.com/mswipe/mob-2.0/commit/54718ed36aa53aff1a01ec97e3fc0d5d77b8e667))
* **MOB-FORM:** de-dupe form design and conflict resolved ([9853ac6](https://gitlab.com/mswipe/mob-2.0/commit/9853ac69efa240f0b3f70514db8e28c2099a900e))
* **MOB-FORM:** discrepancy grid design ([bb98299](https://gitlab.com/mswipe/mob-2.0/commit/bb982995c892bae79bf50cfa2f85da399b29b558))
* **MOB-FORM:** discrepancy grid design ([07f97d3](https://gitlab.com/mswipe/mob-2.0/commit/07f97d346305fc5f353403ec996ce2bd11b6ae4f))
* **Non-STP:** Non STP datatable, Datepicker , Card, etc. UI changes ([a96222b](https://gitlab.com/mswipe/mob-2.0/commit/a96222bf0afae9e0cbd47e9c86a2e5dc8be0481b))


### Bug Fixes

* **Process Module:** COnflict resolved ([ef6534c](https://gitlab.com/mswipe/mob-2.0/commit/ef6534c83681f12d0111c9e21c993423e10fb2c5))

### [0.0.1-beta.4](https://gitlab.com/mswipe/mob-2.0/compare/v0.0.1-beta.3...v0.0.1-beta.4) (2020-07-01)


### Features

* **Application Process  Module:** Module , routing and component created MOB development started ([db425ed](https://gitlab.com/mswipe/mob-2.0/commit/db425edf72f333dae5753af5190f1a92875cbd1c))

### [0.0.1-beta.3](https://gitlab.com/mswipe/mob-2.0/compare/v0.0.1-beta.2...v0.0.1-beta.3) (2020-06-30)
